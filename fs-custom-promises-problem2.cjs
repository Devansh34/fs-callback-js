const fs = require("fs");

function readingFile(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(`./${fileName}`, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function writingFile(fileName,data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(`./${fileName}`, data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve("Write");
      }
    });
  });
}

function appendingFile(fileName, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(`./${fileName}`, data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve("Append");
      }
    });
  });
}

function deletingFile(fileName) {
  return new Promise((resolve, reject) => {
    fs.unlink(`./${fileName}`, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve("Delete");
      }
    });
  });
}

function customPromise2() {
  readingFile("lipsum.txt")
    .then((data) => writingFile("upper.txt", data.toUpperCase()))
    .then(() => writingFile("fileNames.txt", "upper.txt\n"))
    .then(() => readingFile("upper.txt"))
    .then((data) =>
      writingFile("newLine.txt", data.toLowerCase().replaceAll(". ", ".\n"))
    )
    .then(() => appendingFile("fileNames.txt", "newLine.txt\n"))
    .then(() => readingFile("fileNames.txt"))
    .then((data) => {
      const arrayOfFiles = data.trim().split("\n");
      return Promise.all(
        arrayOfFiles.map((fileName) => readingFile(`${fileName}`, "utf-8"))
      );
    })
    .then((data) => {
      const sortedContent = data.sort().join("\n");
      return writingFile("sort.txt", sortedContent);
    })
    .then(() => appendingFile("fileNames.txt", "sort.txt\n"))
    .then(() => readingFile("fileNames.txt"))
    .then((data) => {
      const arrayOfFiles = data.split("\n").filter(Boolean);
      return Promise.all(
        arrayOfFiles.map((fileName) => deletingFile(fileName))
      );
    })
    .catch((err) => console.error(err));
}

customPromise2();
