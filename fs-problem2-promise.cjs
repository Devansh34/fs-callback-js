const fs = require("fs").promises;

function readingFile(fileName) {
  return fs.readFile(`./${fileName}`, "utf-8");
}

function writingFile(fileName, data) {
  return fs.writeFile(`./${fileName}`, data);
}

function appendingFile(fileName, addingParam) {
  return fs.appendFile(`./${fileName}`, addingParam);
}

function deletingFile(fileName) {
  return fs.unlink(`./${fileName}`);
}

function fsProblem2() {
  readingFile("lipsum.txt")
    .then((data) => {
      console.log("Reading lipsum.txt");
      data = data.toUpperCase();
      return writingFile("upper.txt", data);
    })
    .then(() => {
      console.log("Converting data to uppercase");
      return writingFile("filenames.txt", "upper.txt");
    })
    .then(() => {
      console.log("Writing upper.txt to filenames.txt");
      return readingFile("upper.txt");
    })
    .then((data) => {
      console.log("Reading contents of upper.txt");
      data = data.toLowerCase().replaceAll(". ", ".\n");
      return writingFile("newline.txt", data);
    })
    .then(() => {
      console.log("Added content to new file");
      return appendingFile("filenames.txt", "\nnewline.txt");
    })
    .then(() => {
      console.log("Appended newline.txt to filenames.txt");
      return readingFile("upper.txt");
    })
    .then((data) => {
      console.log("Read the contents of upper.txt");
      data = data.split("").sort().join();
      return writingFile("sort.txt", data);
    })
    .then(() => {
      console.log("Written the sorted content of upper");
      return readingFile("newline.txt");
    })
    .then((data) => {
      console.log("Reading contents of newline.txt");
      data = data.split("").sort().join();
      return appendingFile("sort.txt", data);
    })
    .then(() => {
      console.log("appending contents to sort.txt");
      return appendingFile("filenames.txt", "\nsort.txt");
    })
    .then(() => {
      console.log("Adding sort.txt to filenames.txt");
      return readingFile("filenames.txt");
    })
    .then((data) => {
      console.log("Reading the names of file created from filenames.txt");
      const files = data.split("\n");
      return Promise.all(files.map(deletingFile));
    })
    .catch((error) => {
      console.error(error);
    });
}

module.exports = fsProblem2;
