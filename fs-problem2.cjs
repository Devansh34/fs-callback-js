const fs = require("fs");


function readingFile(fileName, callback) {
  fs.readFile(`./${fileName}`, "utf-8", (err, data) => {
    if (err) {
      console.error(err);
    } else {
      callback(data);
    }
  });
}

function writingFile(fileName, data, callback) {
  fs.writeFile(`./${fileName}`, data, (err) => {
    if (err) {
      console.error(err);
    } else {
      callback();
    }
  });
}

function appendingFile(fileName, addingParam, callback) {
  fs.appendFile(`./${fileName}`, addingParam, (err) => {
    if (err) {
      console.error(err);
    } else {
      callback();
    }
  });
}
function deletingFile(fileName) {
  fs.unlink(`./${fileName}`, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("deleted the " + fileName);
    }
  });
}

function fsProblem2() {
  readingFile("lipsum.txt", (data) => {
    console.log("reading the lipsum");
    data = data.toUpperCase();
    writingFile("upper.txt", data, () => {
      console.log("converting to upper case");
      writingFile("filesnames.txt", "upper.txt", () => {
        console.log("Creating the filenames txt and placing upper txt name");
        readingFile("upper.txt", (data1) => {
          console.log("reading the content of upper case file");
          data1 = data1.toLowerCase().replaceAll(". ", ".\n");
          writingFile("newline.txt", data1, () => {
            console.log("writing the content to the new file");
            appendingFile("filesnames.txt", "\nnewline.txt", () => {
              console.log("adding name of file to the filesnames.txt");
              readingFile("upper.txt", (data2) => {
                console.log("reading the upper file");
                data2 = data2.split("").sort().join("");
                writingFile("sort.txt", data2, () => {
                  console.log(
                    "Sorting the content of the file upper and adding to sort file"
                  );
                  readingFile("newline.txt", (data3) => {
                    console.log("reading newline file");
                    data3 = data3.split("").sort().join("");
                    appendingFile("sort.txt", data3, () => {
                      console.log("Adding new line file data to sort file");
                      appendingFile("filesnames.txt", "\nsort.txt", () => {
                        console.log("adding name of file to filesnames file");
                        readingFile("filesnames.txt", (data4) => {
                          console.log(
                            "reading the names of created files in the filesnames.txt"
                          );
                          filless = data4.split("\n");
                          deletingFile(filless[0]);
                          deletingFile(filless[1]);
                          deletingFile(filless[2]);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports=fsProblem2