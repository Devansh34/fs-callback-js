const fs=require('fs')

let dirPath='.'

function creatingFile(fileno,count){
    return new Promise((resolve,reject) => {
        if(fileno<=count){
            let obj={file:fileno}
            fs.writeFile(`${dirPath}/file${fileno}.json`,JSON.stringify(obj),(err) => {
                if(err){
                    reject(err)
                }else{
                    resolve('File created')
                }
            })
        }
    })
}

function deletingFile(fileno){
    return new Promise((resolve,reject) => {
        fs.unlink(`${dirPath}/file${fileno}.json`,(err) => {
            if(err){
                reject(err)
            }else{
                resolve('File deleted')
            }
        })
    })
}


function simultaneously(currFile,count){
    creatingFile(currFile,count)
    .then((data) => {
        console.log("File Created " +currFile )
        return deletingFile(currFile)
    })
    .then(() => {
        console.log("Delete file " + currFile)
        return simultaneously(currFile+1,count)

    })
}

function fsProblem1(dir,count){
    dirPath=dir
    simultaneously(1,count)
}

fsProblem1('.',5)