const fs = require("fs");

function fsProblem1(dirPath, num) {
  createDir(dirPath, (err, data) => {
    if (err) {
      console.error(err);
    } else {
      createFile(dirPath + "/file.json", num, (err) => {
        if (err) {
          console.error(err);
        }
      });
    }
  });
}

function createDir(dirpath, callback) {
  fs.mkdir(dirpath, (err, data) => {
    if (err) {
      console.error(err);
      callback(err, null);
    } else {
      console.log("Directory created");
      callback(null, data);
    }
  });
}

function createFile(filePath, num, callback) {
  if (num == 0) {
    return;
  }

  fs.writeFile(filePath, "", (err) => {
    if (err) {
      console.error(err);
      callback(err);
    } else {
      console.log(`File number ${num} created`);
      callback(null);
      deleteFile(filePath, num, callback);
    }
  });
}

function deleteFile(path, num, callback) {
  fs.unlink(path, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log(`File number ${num} deleted`);
      callback(null);
      num--;
      createFile(path, num, callback);
    }
  });
}

module.exports=fsProblem1