const fs = require("fs").promises;

function fsProblem1(dirPath, num) {
  createDir(dirPath)
    .then(() => createFile(dirPath + "/file.json", num))
    .catch((err) => console.error(err));
}

function createDir(dirPath) {
  return fs
    .mkdir(dirPath)
    .then(() => console.log("Directory Created"))
    .catch((err) => {
      console.error(err);
      throw err;
    });
}

function createFile(filePath, num) {
  if (num == 0) {
    return Promise.resolve();
  }
  return fs
    .writeFile(filePath, "")
    .then(() => {
      console.log(`File $(num) Created`);
      return deleteFile(filePath, num);
    })
    .catch((err) => console.error(err));
}

function deleteFile(filePath,num){
    return fs.unlink(filePath)
    .then(() => {
        console.log(`File ${num} deleted`)
        num--
        createFile(filePath,num)
    })
    .catch((err) => console.error(err))
}

module.exports=fsProblem1